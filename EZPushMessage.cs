﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
namespace Ezviz.OpenSDK
{
    /// <summary>
    /// 推送消息基类
    /// </summary>
    public class EZPushMessage
    {
        /// <summary>
        /// 设备序号
        /// </summary>
        [JsonProperty("deviceSeril")]
        public string DeviceSerial
        {
            get;
            set;
        }
        /// <summary>
        /// 消息时间
        /// </summary>
        [JsonProperty("msgTime")]
        public DateTime MessageTime
        {
            get;
            set;
        }
        /// <summary>
        /// 消息类型
        /// </summary>
        [JsonProperty("msgType")]
        public int MessageType
        {
            get;
            set;
        }
    }
}
