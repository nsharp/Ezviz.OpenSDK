﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Ezviz.OpenSDK
{
    /// <summary>
    /// 服务器时间
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct STREAM_TIME
    {
        /// unsigned int
        public uint iYear;

        /// unsigned int
        public uint iMonth;

        /// unsigned int
        public uint iDay;

        /// unsigned int
        public uint iHour;

        /// unsigned int
        public uint iMinute;

        /// unsigned int
        public uint iSecond;
    }

}
