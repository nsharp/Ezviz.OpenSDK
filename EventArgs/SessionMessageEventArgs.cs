﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ezviz.OpenSDK
{
    /// <summary>
    /// 会话消息事件参数
    /// </summary>
    public class SessionMessageEventArgs:EventArgs
    {
        public SessionMessageEventArgs(OpenSDKSession session, EZMessageType msgType, int errorCode, string errorMessage, IntPtr userData)
        {
            this.Session = session;
            this.MessageType = msgType;
            this.ErrorCode = errorCode;
            this.ErrorMessage = errorMessage;
            this.UserData = userData;
        }

        public OpenSDKSession Session
        {
            get;
        }

        public EZMessageType MessageType
        {
            get;
        }
        public int ErrorCode
        {
            get;
        }
        public string ErrorMessage
        {
            get;
        }
        public IntPtr UserData
        {
            get;
        }
    }
}
