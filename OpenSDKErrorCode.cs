﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ezviz.OpenSDK
{
    /// <summary>
    /// 错误代码
    /// </summary>
    public class OpenSDKErrorCode
    {
        /// <summary>
        /// 没有错误
        /// </summary>
        public const int OK = 0;
        /// <summary>
        /// 常规（通用）错误
        /// </summary>
        public const int GENERAL_ERROR = -1;
        /******************************************************************************************************************/
        /*下列为V2.4.6以前错误码，V2.4.6版本之后将不再使用*/
        /******************************************************************************************************************/
        /// <summary>
        ///  Json解析出错
        /// </summary>
        public const int JSON_ERROR = 10001;
        /// <summary>
        /// 获取平台数据出错 
        /// </summary>
        public const int ERROR = 10002;
        /// <summary>
        ///  不支持的设备     
        /// </summary>
        public const int DEV_NO_SUPPORT = 10003;
        /// <summary>
        ///  申请内存失败     
        /// </summary>
        public const int OPEN_SDK_ALLOC_ERROR = 10004;
        /// <summary>
        /// 传入参数非法     
        /// </summary>
        public const int OPEN_SDK_PARAM_ERROR = 10005;
        /// <summary>
        /// 安全密钥出错     
        /// </summary>
        public const int SAFE_KEY_ERROR = 10006;
        /// <summary>
        /// 录像搜索出错
        /// </summary>
        public const int SEARCHING_ERROR = 10007;
        /// <summary>
        /// 同步参数出错  
        /// </summary>
        public const int SYNC_ERROR = 10008;
        /// <summary>
        /// 接口未实现，主要针对平台
        /// </summary>
        public const int INTERFACE_NO_IMPL = 10009;
        /// <summary>
        /// 接口调用顺序出错
        /// </summary>
        public const int ORDER_ERROR = 10010;
        /// <summary>
        /// SESSION已失效，Session对象被释放
        /// </summary>
        public const int SESSION_EXPIRED = 10011;

        public const int DATA_ACCESS_ERROR = 19999;

        /******************************************************************************************************************/
        /* 下列为V2.4.6新版本错误码，通过OpenSDK_GetLastErrorCode拿到。接口基本默认只返回0和-1（除了部分特殊说明的接口）
        错误码格式{xbbbbb}; x表示错误码表示，x的值如下表：
        /// 0: correct condition
        /// 1: network setup exception;(no DNS; connection lost)
        /// 2: user operation error; input invalid params; rely on lib error(load; init; unload)
        /// 3: platform server error
        /// 4: memory exception; system resource(alloc memory; create thread failed)
        /// 5: opensdk dependent lib netstream errorcode.
        /// 6: method not supported
        /// 9: undefined error
        */
        /******************************************************************************************************************/
        /// <summary>
        /// 给定的远程主机没有得到解析，这里是指platform域名无法正常解析;可能是DNS没有配置或者机器没有连网。
        /// </summary>
        public const int COULDNT_RESOLVE_HOST = 100006;
        /// <summary>
        /// 远程主机不可达，这里是指无法访问platform;可能是platform地址配置错误。
        /// </summary>
        public const int COULDNT_CONNECT = 100007;
        /// <summary>
        ///  请求操作超时; 超时时间为20s; 请求平台超时，请检查platform地址配置错误。
        /// </summary>
        public const int OPERATION_TIMEOUT = 100028;

        /// <summary>
        /// 接口传入参数不符合要求
        /// </summary>
        public const int BAD_PARAMS = 200001;
        /// <summary>
        /// 当前Session不存在或者被释放;可能是SessionId传入值错误或者是Session已经被释放。
        /// </summary>
        public const int SESSION_INVALID = 200002;
        /// <summary>
        /// 指定时间段内录像记录不存在
        /// </summary>
        public const int VIDEO_RECORD_NOT_EXIST = 200003;
        /// <summary>
        ///  录像记录正在搜索
        /// </summary>
        public const int VIDEO_RECORD_SEARCHING = 200004;
        /// <summary>
        /// 关闭告警失败; 可能是没有开启告警或者已经关闭告警
        /// </summary>
        public const int STOP_ALARM_REC_FAILED = 200005;
        /// <summary>
        /// 验证码不正确
        /// </summary>
        public const int PERMANENT_KEY_INVALID = 200006;
        /// <summary>
        /// 图片解码失败
        /// </summary>
        public const int PIC_DECRYPT_FAILED = 200007;
        /// <summary>
        /// 图片内容无效
        /// </summary>
        public const int PIC_CONTENT_INVALID = 200008;
        /// <summary>
        /// 图片无需解码
        /// </summary>
        public const int PIC_NO_NEED_DECRYPT = 200009;
        /// <summary>
        ///  无法分配图片资源内存，可能内存不足或者图片过大
        /// </summary>
        public const int PIC_COULDNT_ALLOC_BUFFERS = 200010;


        /// <summary>
        /// 请求返回的信息;json无法正常解析;可能是PlatformAddr配置有问题
        /// </summary>
        public const int RESPINFO_BAD = 300001;
        /// <summary>
        ///  请求返回信息格式有误
        /// </summary>
        public const int RESPINFO_INVALID = 300002;
        /// <summary>
        ///  accesstoken异常或者过期;accessToken异常或请求方法不存在
        /// </summary>
        public const int ACCESSTOKEN_INVALID = 310002;
        /// <summary>
        /// 表示输入参数有问题。平台显示签名错误
        /// </summary>
        public const int SIGNATURE_ERROR = 310008;
        /// <summary>
        /// APPKEY下对应的第三方userId和phone未绑定
        /// </summary>
        public const int USERID_PHONE_UNBIND = 310014;
        /// <summary>
        /// 通道不存在;通道对应某一监控点
        /// </summary>
        public const int CHANNEL_NOT_EXIST = 320001;
        /// <summary>
        ///  安全认证失败;需进行SDK安全认证; 可能原因是开通萤石云服务后，跨机器或者是AccessToken（通过getAccessToken接口拿到）和AppKey对应不同
        /// </summary>
        public const int IDENTIFY_FAILED = 320005;
        /// <summary>
        /// 该用户不拥有该设备
        /// </summary>
        public const int USER_NOTOWN_DEVICE = 320018;


        /// <summary>
        /// 创建线程失败
        /// </summary>
        public const int COULDNT_CREATE_THREAD = 400001;
        /// <summary>
        /// 申请内存资源失败
        /// </summary>
        public const int COULDNT_ALLOC_BUFFERS = 400002;

        /// <summary>
        /// 不支持非1.7设备
        /// </summary>
        public const int DEV_NOT_SUPPORT = 600001;
        /// <summary>
        /// 接口未实现
        /// </summary>
        public const int API_NO_IMPLEMENT = 600002;

        /// <summary>
        ///  传入参数非法
        /// </summary>
        public const int PUSH_PARAM_ERROR = 710001;
        /// <summary>
        /// 数据未初始化，请先调用Init接口初始化
        /// </summary>
        public const int PUSH_DATA_UNINIT_ERROR = 710002;
        /// <summary>
        ///  未向Push平台注册，即未调register接口
        /// </summary>
        public const int PUSH_NO_REGISTER_ERROR = 710003;
        /// <summary>
        /// 未创建创建推送对象，即未调create接口
        /// </summary>
        public const int PUSH_NO_MQTT_CREATE_ERROR = 710004;
        /// <summary>
        /// sdk同push服务器断开连接
        /// </summary>
        public const int PUSH_MQTT_DISCONNECTED_ERROR = 720003;
        /// <summary>
        /// 达到消息接收上限
        /// </summary>
        public const int PUSH_MQTT_MAX_MESSAGES_ERROR = 720004;
        /// <summary>
        /// 不合法的UTF-8字符串
        /// </summary>
        public const int PUSH_MQTT_BAD_UTF8_STRING_ERROR = 720005;
        /// <summary>
        /// 传入参数为空指针
        /// </summary>
        public const int PUSH_MQTT_NULL_PARAMETER_ERROR = 720006;

        /// <summary>
        /// 连接失败，协议版本不支持
        /// </summary>
        public const int PUSH_MQTT_VERSION_INVALID_ERROR = 730001;
        /// <summary>
        /// 连接失败，唯一标识不正确
        /// </summary>
        public const int PUSH_MQTT_IDENTIFIER_ERROR = 730002;
        /// <summary>
        /// 连接失败，服务不存在
        /// </summary>
        public const int PUSH_MQTT_SERVER_UNAVAILABLE_ERROR = 730003;
        /// <summary>
        ///  连接失败，mqtt用户名和密码不正确
        /// </summary>
        public const int PUSH_MQTT_BAD_USERNAME_PASSWORD_ERROR = 730004;
        /// <summary>
        /// 连接失败，未授权
        /// </summary>
        public const int PUSH_MQTT_NOT_AUTHORIZED_ERROR = 730005;

        /// <summary>
        ///  请求返回的信息;json无法正常解析;可能是PlatformAddr配置有问题
        /// </summary>
        public const int PUSH_PLATFORM_RESPINFO_BAD = 740001;
        /// <summary>
        /// 请求返回信息格式有误
        /// </summary>
        public const int PUSH_PLATFORM_RESPINFO_INVALID = 740002;
        /// <summary>
        /// Session Invalid
        /// </summary>
        public const int PUSH_PLATFORM_SESSION_INVALID_ERROR = 740003;
        /// <summary>
        /// Bad credentials; 可能是PushId有误或者不支持
        /// </summary>
        public const int PUSH_PLATFORM_UNAUTHORIZED_ERROR = 740401;

        /// <summary>
        ///  创建线程失败
        /// </summary>
        public const int PUSH_COULDNT_CREATE_THREAD = 750001;
        /// <summary>
        /// 申请内存资源失败
        /// </summary>
        public const int PUSH_COULDNT_ALLOC_BUFFERS = 750002;
        /// <summary>
        /// 给定的远程主机没有得到解析，这里是指platform域名无法正常解析;可能是DNS没有配置或者机器没有连网。
        /// </summary>
        public const int PUSH_COULDNT_RESOLVE_HOST = 760006;
        /// <summary>
        /// 远程主机不可达，这里是指无法访问platform;可能是platform地址配置错误。
        /// </summary>
        public const int PUSH_COULDNT_CONNECT = 760007;
        /// <summary>
        /// 请求操作超时; 超时时间为20s; 请求平台超时，请检查platform地址配置错误。
        /// </summary>
        public const int PUSH_OPERATION_TIMEOUT = 760028;

        /// <summary>
        /// 对讲开启传入参数有误
        /// </summary>
        public const int START_TALK_FAILED = 500001;
        /// <summary>
        /// 对讲已经开启
        /// </summary>
        public const int TALK_OPENED = 500002;

        /// <summary>
        /// AllocSession 失败
        /// </summary>
        public const int ALLOCSESSION_FAILED = 900001;
        /// <summary>
        /// 查询回放记录失败
        /// </summary>
        public const int SEARCH_RECORD_FAILED = 900002;
        /// <summary>
        /// 开启告警失败
        /// </summary>
        public const int START_ALARM_REC_FAILED = 900003;


        /// <summary>
        /// 获取操作码失败， 一般是由于用户开启终端绑定了，会报这个错误。该用户没有关联硬件特征码;设备操作码and密钥获取失败;错误码为:3128
        /// </summary>
        public const int INS_ERROR_OPERATIONCODE_FAILED = 45;
        /// <summary>
        /// 取流路数限制， 即设备达到连接上线(直连3路，流媒体由服务器配置)
        /// </summary>
        public const int STREAM_LIMIT = 410;
        /// <summary>
        /// 设备不在线
        /// </summary>
        public const int DEV_NO_ONLINE = 3121;
        /// <summary>
        ///  重新输入密钥  1.明文密钥和输入MD5密钥不相等
        /// </summary>
        public const int PERMANENTKEY_EXCEPTION = 2012;
        /// <summary>
        /// 流媒体向设备发送或接受信令超时;查看设备连接网络是否稳定。一般是网络问题。
        /// </summary>
        public const int VTDU_TIMEOUT = 2021;
        /// <summary>
        /// vtdu客户端接收回应超时;没有视频源;可能摄像头与后端设备接触有问题。
        /// </summary>
        public const int VTDU_CLIENT_TIMEOUT = 2025;

        /// <summary>
        /// vtdu取流停止失败
        /// </summary>
        public const int VTDU_STOP = 2034;
        /// <summary>
        ///  VTDU 客户端连接不上VTM;预览时候断网;包重连异常
        /// </summary>
        public const int VTDU_TOKEN_NOCONNECT_VTM = 2047;
        /// <summary>
        ///  VTDU 客户端连接不上VTDU
        /// </summary>
        public const int VTDU_TOKEN_NOCONNECT_VTDU = 2048;
        /// <summary>
        /// < 设备连接不上流媒体;基本判断为网络问题引起。
        /// </summary>
        public const int DEVICE_UNCONNECT_VTDU = 2051;
        /// <summary>
        /// 没有关联特征码
        /// </summary>
        public const int PLATFORM_CLIENT_NO_SIGN_RELEATED = 3128;
        /// <summary>
        ///  TTS设备不在线
        /// </summary>
        public const int TTS_DEV_NO_ONLINE = 5012;
        /// <summary>
        /// 获取操作码参数错误;开启终端绑定，且没有验证通过就进行预览。
        /// </summary>
        public const int INS_ERROR_V17_GET_OPERATIONCODE_PARAMETER_ERROR = 2056;
        /// <summary>
        ///  网络带宽受限或者短时间对同一个请求过于频繁
        /// </summary>
        public const int PRIVATE_VTDU_REQUEST_TIMEOUT = 2225;
        /// <summary>
        /// < 网络无法联通
        /// </summary>
        public const int PRIVATE_VTDU_DISCONNECTED_LINK = 2226;
        /// <summary>
        /// < token无权限;过期失效
        /// </summary>
        public const int PRIVATE_VTDU_STATUS_411 = 5411;
        /// <summary>
        /// 设备连接预览流媒体服务器失败;设备同vtdu不在同一个局域网
        /// </summary>
        public const int PRIVATE_VTDU_STATUS_452 = 5452;
        /// <summary>
        /// 并发路数限制
        /// </summary>
        public const int PRIVATE_VTDU_STATUS_546 = 5546;
    }
}
