﻿using System.Text;
using System.Security.Cryptography;

namespace System
{
    static class StringExtension
    {
        private static MD5CryptoServiceProvider MD5 = new MD5CryptoServiceProvider();

        public static byte[] ToMD5(this string str,Encoding encoding)
        {
            if (string.IsNullOrEmpty(str))
                return new byte[0];
            return MD5.ComputeHash(encoding.GetBytes(str));
        }

        public static byte[] ToMD5(this string str)
        {
            return str.ToMD5(Encoding.UTF8);
        }
    }
}
