﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
namespace Ezviz.OpenSDK
{
    /// <summary>
    /// 获取Camera结果
    /// </summary>
    public class EZCameraResponse
    {
        public EZCameraResponse()
        {
        }
        [JsonProperty("result")]
        public CameraResult Result
        {
            get;
            internal set;
        }
        /// <summary>
        /// 摄像头结果
        /// </summary>
        public class CameraResult 
        {
            public CameraResult()
            {
            }

            /// <summary>
            /// 错误码
            /// </summary>
            [JsonProperty("code")]
            public EZErrorCode ErrorCode
            {
                get;
                internal set;
            }
            /// <summary>
            /// 错误消息
            /// </summary>
            [JsonProperty("msg")]
            public string ErrorMessage
            {
                get;
                internal set;
            }

            /// <summary>
            /// 摄像头列表
            /// </summary>
            [JsonProperty("data")]
            public Camera[] Cameras
            {
                get;
                internal set;
            }
        }
        /// <summary>
        /// 摄像头信息
        /// </summary>
        public class Camera
        {
            /// <summary>
            /// 设备ID
            /// </summary>
            [JsonProperty("deviceId")]
            public string DeviceId
            {
                get;
                internal set;
            }
            /// <summary>
            /// 设备序号
            /// </summary>
            [JsonProperty("deviceSerial")]
            public string DeviceSerial
            {
                get;
                internal set;
            }
            /// <summary>
            /// 设备名称
            /// </summary>
            [JsonProperty("deviceName")]
            public string DeviceName
            {
                get;
                internal set;
            }
            /// <summary>
            /// 监控点ID
            /// </summary>
            [JsonProperty("cameraId")]
            public string CameraId
            {
                get;
                internal set;
            }
            /// <summary>
            /// 监控点编号
            /// </summary>
            [JsonProperty("cameraNo")]
            public int CameraNo
            {
                get;
                internal set;
            }
            /// <summary>
            /// 监控点名称
            /// </summary>
            [JsonProperty("cameraName")]
            public string CameraName
            {
                get;
                internal set;
            }
            /// <summary>
            /// 监控点在线状态
            /// </summary>
            [JsonProperty("status")]
            public bool Online
            {
                get;
                internal set;
            }
            /// <summary>
            /// 设备分享状态,0-未共享;1-共享所有者;2-共享接受者
            /// </summary>
            [JsonProperty("isShared")]
            public int IsShared
            {
                get;
                internal set;
            }
            /// <summary>
            /// 封面图片URL
            /// </summary>
            [JsonProperty("picUrl")]
            public string PicUrl
            {
                get;
                internal set;
            }
            /// <summary>
            /// 是否加密
            /// </summary>
            [JsonProperty("isEncrypt")]
            public bool IsEncrypt
            {
                get;
                internal set;
            }
        }
    }
}
