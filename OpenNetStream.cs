﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Ezviz.OpenSDK
{
    /// <summary>
    /// 消息处理器
    /// </summary>
    /// <param name="session">会话</param>
    /// <param name="msgType">消息类型</param>
    /// <param name="errorCode"></param>
    /// <param name="message"></param>
    /// <param name="user"></param>
    internal delegate void MessageHandler(IntPtr session, EZMessageType msgType, uint errorCode, string message, IntPtr userData);
    /// <summary>
    /// 报警消息推送回调函数
    /// </summary>
    /// <param name="cameraId">监控点Id</param>
    /// <param name="content">报警推送内容</param>
    /// <param name="alarmTime">报警推送时间</param>
    /// <param name="userData">用户自定义数据</param>
    public delegate void AlarmMessageHandler(string cameraId, string content, string alarmTime, IntPtr userData);
    /// <summary>
    /// 推送消息回调函数(此功能已经废弃, OpenSDK_Alarm_SetMsgCallBack的第二个参数设置为NULL)
    /// </summary>
    /// <param name="content">消息推送内容</param>
    /// <param name="userData">用户自定义数据</param>
    public delegate void PublishMessageHandler(string content, IntPtr userData);
    /// <summary>
    /// 报警消息透传回调函数
    /// </summary>
    /// <param name="content"></param>
    /// <param name="userData"></param>
    public delegate void AlarmTransparentHandler(string content, IntPtr userData);
    /// <summary>
    /// 告警通知
    /// </summary>
    /// <param name="notifyType">告警通知类型，比方连接异常、重连成功、重连失败</param>
    /// <param name="errorCode">错误码</param>
    /// <param name="userData">用户自定义数据，回调函数会原样抛出</param>
    public delegate void AlarmNotifyHandler(EZAlarmNotifyType notifyType, int errorCode, IntPtr userData);

    /// <summary>
    /// 报警消息推送回调函数
    /// </summary>
    /// <param name="desc">推送描述信息</param>
    /// <param name="content">推送内容</param>
    /// <param name="detail">完整的推送信息</param>
    /// <param name="userData">用户自定义数据</param>
    public delegate void PushMessageHandler(string desc, string content, string detail, IntPtr userData);

    /// <summary>
    /// 数据回调格式
    /// </summary>
    /// <param name="dataType">数据类型</param>
    /// <param name="data">数据内容</param>
    /// <param name="len">数据长度 </param>
    /// <param name="userData">用户自定义数据 </param>
    public delegate void DataCallBack(EZDataType dataType, IntPtr data, int len,IntPtr userData);


    /// <summary>
    /// OPEN SDK 接口库
    /// </summary>
    internal static class OpenNetStream
    {
        /// <summary>
        /// 初始化库
        /// </summary>
        /// <param name="appId">AppID</param>
        /// <returns></returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_Init(string appId);
        /// <summary>
        /// 释放库
        /// </summary>
        /// <returns></returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_FiniLib();

        /// <summary>
        /// 申请一个会话
        /// </summary>
        /// <param name="handle"> 设置回调函数</param>
        /// <param name="user">用户自定义数据</param>
        /// <param name="session">用于接收分配的会话</param>
        /// <param name="sessionLen">会话的长度</param>
        /// <param name="sync">是否使用同步模式</param>
        /// <param name="timeOut">同步等待时长,默认0xEA60=1min</param>
        /// <returns> 0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_AllocSession(MessageHandler handler, IntPtr user, ref IntPtr session, ref int sessionLen, bool sync = false, uint timeOut = 0xEA60);

        /// <summary>
        /// 销毁SDK操作句柄
        /// </summary>
        /// <param name="session">会话，通过OpenSDK_AllocSession()创建</param>
        /// <returns>0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_FreeSession(IntPtr session);

        /// <summary>
        /// 获取系统时间
        /// </summary>
        /// <param name="session"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        [DllImport("OpenNetStream.dll", CharSet = CharSet.Ansi)]
        public static extern int OpenSDK_GetOSDTime(IntPtr session,[In,Out]ref  STREAM_TIME time);

        /// <summary>
        /// 播放视频
        /// </summary>
        /// <param name="session">会话</param>
        /// <param name="playWnd">播放窗口句柄</param>
        /// <param name="cameraId">监控点ID </param>
        /// <param name="accessToken">认证token</param>
        /// <param name="videoLevel">视频质量，输入范围在0-2之间 </param>
        /// <param name="safeKey">视频加密密钥 </param>
        /// <param name="appKey">平台AppKey </param>
        /// <param name="nSCBMsg">同步回调传出参数,如果为同步，不允许为NULL </param>
        /// <returns></returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_StartRealPlay(IntPtr session, IntPtr playWnd, string cameraId, string accessToken, VideoLevel videoLevel, string safeKey, string appKey,ref NSCBMsg nSCBMsg);
        /// <summary>
        /// 停止预览
        /// </summary>
        /// <param name="session">会话</param>
        /// <param name="nSCBMsg">同步回调传出参数,如果为同步，不允许为NULL</param>
        /// <returns>0表示成功，非0表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_StopRealPlay(IntPtr session,ref NSCBMsg nSCBMsg);

        /// <summary>
        /// 设置数据回调
        /// </summary>
        /// <param name="session">会话</param>
        /// <param name="dataCallBack">回调函数 </param>
        /// <param name="userData">用户自定义数据，会通过DataCallBack原样抛出 </param>
        /// <returns></returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_SetDataCallBack(IntPtr session, DataCallBack dataCallBack, IntPtr userData);

        /// <summary>
        /// 回放
        /// </summary>
        /// <param name="session">会话</param>
        /// <param name="playWnd">播放窗口句柄</param>
        /// <param name="cameraId">监控点ID</param>
        /// <param name="accessToken">认证Token</param>
        /// <param name="safeKey">视频加密密钥</param>
        /// <param name="startTime">开始时间</param>
        /// <param name="stopTime">停止时间</param>
        /// <param name="appKey">AppKey</param>
        /// <param name="nSCBMsg">同步回调传出参数,如果为同步，不允许为NULL</param>
        /// <returns> 0表示成功，非0表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_StartPlayBack(IntPtr session, IntPtr playWnd, string cameraId, string accessToken, string safeKey, string startTime, string stopTime, string appKey, NSCBMsg nSCBMsg = null);

        /// <summary>
        /// 暂停回放
        /// </summary>
        /// <param name="session">会话</param>
        /// <returns>0表示成功，非0表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_PlayBackPause(IntPtr session);
        /// <summary>
        /// 恢复回放
        /// </summary>
        /// <param name="session">会话</param>
        /// <returns>0表示成功，非0表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_PlayBackResume(IntPtr session);
        /// <summary>
        /// 停止回放
        /// </summary>
        /// <param name="session">会话</param>
        /// <returns>0表示成功，非0表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_StopPlayBack(IntPtr session, NSCBMsg nSCBMsg = null);

        /// <summary>
        /// 录像搜索，包含云存储和设备SD卡录像
        /// </summary>
        /// <param name="session">会话Id</param>
        /// <param name="cameraId">监控点Id</param>
        /// <param name="accessToken">认证Token</param>
        /// <param name="startTime">开始时间</param>
        /// <param name="stopTime">停止时间 </param>
        /// <param name="nSCBMsg">步回调传出参数,如果为同步，不允许为NULL</param>
        /// <returns> 0表示成功，非0表示失败 </returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_StartSearch(IntPtr session, string cameraId, string accessToken, string startTime, string stopTime, NSCBMsg nSCBMsg = null);


        /// <summary>
        /// 打开声音
        /// </summary>
        /// <param name="session">会话</param>
        /// <returns>0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_OpenSound(IntPtr session);
        /// <summary>
        /// 关闭声音
        /// </summary>
        /// <param name="session">会话</param>
        /// <returns>0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_CloseSound(IntPtr session);


        /// <summary>
        /// 获取音量
        /// </summary>
        /// <param name="session">会话</param>
        /// <returns>0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern ushort OpenSDK_GetVolume(IntPtr session);
        /// <summary>
        /// 设置音量
        /// </summary>
        /// <param name="session">会话</param>
        /// <param name="volume">音量大小，0-100之间</param>
        /// <returns>0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern ushort OpenSDK_SetVolume(IntPtr session, ushort volume);
        /// <summary>
        /// 开启语音对讲, 不支持多个设备同时对接
        /// </summary>
        /// <param name="session">会话</param>
        /// <param name="accessToken">认证Token</param>
        /// <param name="cameraId">摄像头Id</param>
        /// <returns>0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_StartVoiceTalk(IntPtr session, string accessToken, string cameraId);
        /// <summary>
        /// 结束语音对讲
        /// </summary>
        /// <param name="session">会话</param>
        /// <returns>0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_StopVoiceTalk(IntPtr session);


        /// <summary>
        ///  截屏，StartRealPlay成功（回调函数中收到MsgId == INS_PLAY_START）情况下使用
        /// </summary>
        /// <param name="session">会话</param>
        /// <param name="filename">文件名</param>
        /// <returns>0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_CapturePicture(IntPtr session, string filename);

        /// <summary>
        ///  登陆接口
        /// </summary>
        /// <param name="accessToken">认证Token</param>
        /// <param name="accessTokenLen">认证Token长度</param>
        /// <returns>0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_Mid_Login(ref IntPtr accessToken, ref int accessTokenLen);

        /// <summary>
        /// 设备添加
        /// </summary>
        /// <param name="accessToken">认证Token</param>
        /// <returns>0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_Mid_Device_Add(string accessToken);

        /// <summary>
        /// 设备操作
        /// </summary>
        /// <param name="accessToken">认证Token</param>
        /// <param name="deviceId">设备ID</param>
        /// <returns>0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_Mid_Device_Oper(string accessToken,string deviceId);

        /// <summary>
        /// 销毁SDK分配的内存
        /// </summary>
        /// <param name="buf">SDK分配的内存</param>
        /// <returns>0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_Data_Free(IntPtr buf);

        /// <summary>
        /// 获取摄像头列表
        /// </summary>
        /// <param name="accessToken">认证Token</param>
        /// <param name="pageStart">分页起始页，从0开始</param>
        /// <param name="pageSize"> 分页大小</param>
        /// <param name="buf">摄像头列表的JSON字符串</param>
        /// <param name="length"> 获取到的数据大小</param>
        /// <returns>0表示成功，-1表示失败 </returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_Data_GetDevList( string accessToken, int pageStart, int pageSize,ref IntPtr buf, ref int length);

        /// <summary>
        /// 获取摄像头信息
        /// </summary>
        /// <param name="accessToken">认证Token</param>
        /// <param name="deviceSerial">设备序列号</param>
        /// <param name="buf">摄像头列表的JSON字符串</param>
        /// <param name="length"> 获取到的数据大小</param>
        /// <returns>0表示成功，-1表示失败 </returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_Data_GetDeviceInfo(string accessToken, string deviceSerial, ref IntPtr buf, ref int length);
        /// <summary>
        ///  获取报警列表
        /// </summary>
        /// <param name="accessToken">认证Token</param>
        /// <param name="cameraId">监控点Id</param>
        /// <param name="startTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <param name="alarmType">报警类型</param>
        /// <param name="status">报警状态，0表示未读，1表示已读，2表示所有</param>
        /// <param name="pageIndex">分页起始页，从0开始</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="buf">报警信息列表</param>
        /// <param name="length">报警信息列表长度</param>
        /// <returns> 0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_Data_GetAlarmList(string accessToken, string cameraId,string startTime,string endTime,EZAlarmType alarmType, EZAlarmStatus status,int pageIndex,int pageSize,  ref IntPtr buf, ref int length);

        /// <summary>
        /// 解密告警图片(建议加密的图片才调用，非加密图片直接下载，可以通过url里面isEncrypted=1来区分
        /// </summary>
        /// <param name="accessToken">认证Token</param>
        /// <param name="picURL">图片URL</param>
        /// <param name="deviceSerail">告警图片对应的设备序列号</param>
        /// <param name="safeKey">解密密钥，默认是设备验证码</param>
        /// <param name="picBuf">解密后图片内容（需要调用OpenSDK_Data_Free释放内存）</param>
        /// <param name="length">picBuf的长度</param>
        /// <returns> 0表示成功，-1表示失败, 失败时调用GetLastErrorCode()</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_DecryptPicture(string accessToken, string picURL, string deviceSerail, string safeKey,  ref IntPtr picBuf, ref int length);

        /// <summary>
        /// 更新监控点详细信息到缓存
        /// </summary>
        /// <param name="accessToken">认证Token</param>
        /// <param name="cameraId">监控点ID</param>
        /// <param name="isEncrypt">监控点设备是否加密</param>
        /// <returns>0表示成功，其他值表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_UpdateCameraInfoToLocal(string accessToken, string cameraId,ref bool isEncrypt);

        /// <summary>
        ///  获取设备详情监控点详细信息
        /// </summary>
        /// <param name="accessToken">认证Token</param>
        /// <param name="cameraId">监控点ID</param>
        /// <param name="buf">监控点信息，需要调用OpenSDK_FreeData接口释放</param>
        /// <param name="len">监控点信息的长度</param>
        /// <returns></returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_GetCameraInfo(string accessToken, string cameraId, IntPtr buf,int len);

        /// <summary>
        /// 设置报警已读
        /// </summary>
        /// <param name="accessToken">认证Token</param>
        /// <param name="alarmId">报警ID</param>
        /// <returns>0表示成功，非0表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_Data_SetAlarmRead(string accessToken, string alarmId);

        /// <summary>
        ///  删除设备
        /// </summary>
        /// <param name="accessToken">认证Token</param>
        /// <param name="deviceId">设备Id</param>
        /// <returns>0表示成功，非0表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_Data_DeleteDevice(string accessToken, string deviceId);

        /// <summary>
        /// 设置告警消息以及推送消息
        /// </summary>
        /// <param name="alarmMessageHandler">告警推送函数</param>
        /// <param name="publishMessageHandler">推送消息函数</param>
        /// <param name="userData">用户自定义数据，回调函数会原样抛出</param>
        /// <returns>0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_Alarm_SetMsgCallBack(AlarmMessageHandler alarmMessageHandler, PublishMessageHandler publishMessageHandler,IntPtr userData);

        /// <summary>
        /// 设置消息透传回调
        /// </summary>
        /// <param name="alarmTransparentHandler">告警透传函数</param>
        /// <param name="userData">用户自定义数据，回调函数会原样抛出</param>
        /// <returns>0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_Alarm_SetTransparentCallBack(AlarmTransparentHandler alarmTransparentHandler,  IntPtr userData);

        /// <summary>
        /// 设置告警通知回调
        /// </summary>
        /// <param name="alarmNotifyHandler">告警通知函数</param>
        /// <param name="userData">用户自定义数据，回调函数会原样抛出</param>
        /// <returns>0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_Alarm_SetNotifyCallBack(AlarmNotifyHandler alarmNotifyHandler, IntPtr userData);

        /// <summary>
        /// 开始接受告警
        /// </summary>
        /// <param name="accessToken">认证Token</param>
        /// <returns>0表示成功，非0表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_Alarm_StartRecv(string accessToken);
        
        /// <summary>
        /// 停止接收告警
        /// </summary>
        /// <returns>0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_Alarm_StopRecv();

        /// <summary>
        /// 云台控制
        /// </summary>
        /// <param name="session"> 会话</param>
        /// <param name="accessToken">认证Token</param>
        /// <param name="cameraId">监控点Id</param>
        /// <param name="command">云台控制命令</param>
        /// <param name="action">云台操作命令</param>
        /// <param name="speed">速度0-7之间</param>
        /// <param name="nSCBMsg"> 同步回调传出参数,如果为同步，不允许为NULL</param>
        /// <returns> 0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_PTZCtrl(IntPtr session,string accessToken, string cameraId, PTZCommand command, PTZAction action, int speed, NSCBMsg nSCBMsg = null);

        /// <summary>
        /// 按监控点ID进行布撤防
        /// </summary>
        /// <param name="session">会话</param>
        /// <param name="accessToken">认证Token</param>
        /// <param name="cameraId">监控点Id</param>
        /// <param name="type">布撤防类型</param>
        /// <param name="status">布撤防状态</param>
        /// <param name="actor">布撤防设备类型</param>
        /// <param name="nSCBMsg"> 同步回调传出参数,如果为同步，不允许为NULL</param>
        /// <returns> 0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_DevDefence(IntPtr session, string accessToken, string cameraId, EZDefenceType type, EZDefenceStatus status, EZDefenceActor actor, NSCBMsg nSCBMsg = null);

        /// <summary>
        /// 按设备ID和通道号进行布撤防
        /// </summary>
        /// <param name="session">会话ID</param>
        /// <param name="accessToken">认证Token</param>
        /// <param name="deviceId">设备Id</param>
        /// <param name="cameraNo">通道号，操作设备本身通道号为0</param>
        /// <param name="type">布撤防类型</param>
        /// <param name="status">布撤防状态</param>
        /// <param name="actor">布撤防设备类型</param>
        /// <param name="nSCBMsg"> 同步回调传出参数,如果为同步，不允许为NULL</param>
        /// <returns>0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_DevDefenceByDev(IntPtr session, string accessToken, string deviceId,int cameraNo, EZDefenceType type, EZDefenceStatus status, EZDefenceActor actor, NSCBMsg nSCBMsg = null);

        /// <summary>
        /// Http请求接口
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="headerParam">头部参数</param>
        /// <param name="body">Body数据</param>
        /// <param name="buf">返回报文的内容</param>
        /// <param name="length">返回报文的长度</param>
        /// <returns>0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_HttpSendWithWait(string url,string headerParam,string body,ref IntPtr buf,ref int len);

        /// <summary>
        /// 获取单个设备信息
        /// </summary>
        /// <param name="accessToken">认证Token</param>
        /// <param name="deviceSerial">设备序列号</param>
        /// <param name="buf">设备信息（需要调用OpenSDK_Data_Free释放内存）</param>
        /// <param name="len">buf的长度</param>
        /// <returns>0表示成功，-1表示失败, 失败时调用GetLastErrorCode()</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_GetSingleDeviceInfo(string accessToken, string deviceSerial,ref IntPtr buf, ref int len);

        /// <summary>
        /// 对设备布撤防
        /// </summary>
        /// <param name="accessToken">认证Token</param>
        /// <param name="deviceSerial">设备序列号</param>
        /// <param name="defence">布撤防, true 布防; false 撤防</param>
        /// <returns>0表示成功，-1表示失败, 失败时调用GetLastErrorCode()</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_DefenceDeviceBySerial(string accessToken, string deviceSerial,[MarshalAs(UnmanagedType.I1)]bool defence);
        /// <summary>
        /// 添加设备
        /// </summary>
        /// <param name="accessToken">认证Token</param>
        /// <param name="deviceSerial">设备序列号</param>
        /// <param name="safeKey">设备验证码</param>
        /// <returns>0表示成功，-1表示失败, 失败时调用GetLastErrorCode()</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_AddDevice(string accessToken, string deviceSerial, string safeKey);

        /// <summary>
        ///  获取开通萤石云服务的短信验证码
        /// </summary>
        /// <param name="phone">开通萤石服务账号手机号码</param>
        /// <returns> 0表示成功，-1表示失败, 失败时调用GetLastErrorCode()</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_GetAuthSmsOfYSService(string phone);

        /// <summary>
        /// 透传萤石云平台接口
        /// </summary>
        /// <param name="phone">开通萤石服务账号手机号码</param>
        /// <param name="smsCode">通过OpenSDK_GetAuthSmsOfYSService接口获取的短信验证码</param>
        /// <returns> 0表示成功，-1表示失败, 失败时调用GetLastErrorCode()</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_OpenYSService(string phone, string smsCode);
        /// <summary>
        ///  设置告警推送回调
        /// </summary>
        /// <param name="handle">告警推送函数</param>
        /// <param name="userData">用户自定义数据，回调函数会原样抛出</param>
        /// <returns>0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_Push_SetAlarmCallBack(PushMessageHandler handle, IntPtr userData);

        /// <summary>
        /// 设置设备上下线状态推送
        /// </summary>
        /// <param name="handle">设备状态推送函数</param>
        /// <param name="userData">用户自定义数据，回调函数会原样抛出</param>
        /// <returns>0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_Push_SetDeviceStatusCallBack(PushMessageHandler handle, IntPtr userData);

        /// <summary>
        /// 设置设备上下线状态推送
        /// </summary>
        /// <param name="handle">告警推送函数</param>
        /// <param name="userData">用户自定义数据，回调函数会原样抛出</param>
        /// <returns>0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_Push_SetTransparentChannelCallBack(PushMessageHandler handle, IntPtr userData);

        /// <summary>
        /// 开始接收推送
        /// </summary>
        /// <param name="accessToken">认证Token</param>
        /// <param name="pushId">接入Push的标识，默认使用开发平台的AppKey</param>
        /// <param name="pushSecret">接入Push所使用密钥，创建应用的时候有平台生成</param>
        /// <returns>0表示成功，-1表示失败, 失败时调用GetLastErrorCode()</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_Push_StartRecv(string pushId,string pushSecret, string accessToken);
        /// <summary>
        /// 停止接收告警
        /// </summary>
        /// <returns> 0表示成功，-1表示失败</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_Push_StopRecv();
        /// <summary>
        /// 获取错误码接口
        /// </summary>
        /// <returns>错误码</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern int OpenSDK_GetLastErrorCode();

        /// <summary>
        /// 获取错误码接口
        /// </summary>
        /// <returns>错误码</returns>
        [DllImport("OpenNetStream.dll")]
        public static extern IntPtr OpenSDK_GetLastErrorDesc();
    }
}

