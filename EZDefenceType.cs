﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ezviz.OpenSDK
{
    /// <summary>
    /// 布撤防报警类型
    /// </summary>
    public enum EZDefenceType
    {
        /// <summary>
        /// 红外
        /// </summary>
        PIR,
        /// <summary>
        /// 在家，A1设备
        /// </summary>
        ATHOME,
        /// <summary>
        /// 外出
        /// </summary>
        OUTDOOR,
        /// <summary>
        /// 婴儿啼哭
        /// </summary>
        BABYCRY,
        /// <summary>
        /// 移动侦测
        /// </summary>
        MOTIONDETECT,
        /// <summary>
        ///  全部
        /// </summary>
        GLOBAL
    }
}
