﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
namespace Ezviz.OpenSDK
{
    public class EZPushDeviceMessage : EZPushMessage
    {
        [JsonProperty("devMsgType")]
        public int DevMsgType
        {
            get;
            set;
        }
        [JsonProperty("channelID")]
        public int ChannelID
        {
            get;
            set;
        }
    }
}
