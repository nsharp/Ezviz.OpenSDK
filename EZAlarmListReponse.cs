﻿using System;
using Newtonsoft.Json;

namespace Ezviz.OpenSDK
{
    /// <summary>
    /// 警情列表结果
    /// </summary>
    public class EZAlarmListReponse
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [JsonProperty("resultCode")]
        public EZErrorCode ErrorCode
        {
            get;
            internal set;
        }
        /// <summary>
        /// 设备数量
        /// </summary>
        [JsonProperty("count")]
        public int Count
        {
            get;
            internal set;
        }
        /// <summary>
        /// 警情列表
        /// </summary>
        [JsonProperty("alarmList")]
        public Alarm[] Alarms
        {
            get;
            internal set;
        }
        /// <summary>
        /// 警情信息
        /// </summary>
        public class Alarm
        {
            /// <summary>
            /// 告警编号
            /// </summary>
            [JsonProperty("alarmId")]
            public string AlarmId
            {
                get;
                internal set;
            }
            /// <summary>
            /// 告警名称
            /// </summary>
            [JsonProperty("alarmName")]
            public string AlarmName
            {
                get;
                internal set;
            }

            /// <summary>
            /// alarmPicUrl
            /// </summary>
            [JsonProperty("alarmPicUrl")]
            public string AlarmPicUrl
            {
                get;
                internal set;
            }

            /// <summary>
            /// 告警触发时间
            /// </summary>
            [JsonProperty("alarmStart")]
            public DateTime AlarmStart
            {
                get;
                internal set;
            }
            /// <summary>
            ///  告警类型
            /// </summary>
            public EZAlarmType AlarmType
            {
                get;
                internal set;
            }
            /// <summary>
            /// 设备序列号
            /// </summary>
            [JsonProperty("deviceSerial")]
            public string DeviceSerial
            {
                get;
                internal set;
            }
            /// <summary>
            ///  设备通道号
            /// </summary>
            [JsonProperty("channelNo")]
            public int ChannelNo
            {
                get;
                internal set;
            }
            /// <summary>
            /// 延迟时间
            /// </summary>
            [JsonProperty("DelayTime")]
            public int DelayTime
            {
                get;
                internal set;
            }
            /// <summary>
            /// 
            /// </summary>
            [JsonProperty("isChecked")]
            public bool IsChecked
            {
                get;
                internal set;
            }
          
            /// <summary>
            /// 加密状态
            /// </summary>
            [JsonProperty("isEncrypt")]
            public bool IsEncrypt
            {
                get;
                internal set;
            }
           
            [JsonProperty("preTime")]
            public int PreTime
            {
                get;
                internal set;
            }
            /// <summary>
            /// 自定义信息 
            /// </summary>
            [JsonProperty("customerInfo")]
            public string CustomerInfo
            {
                get;
                internal set;
            }
            /// <summary>
            /// 自定义信息 
            /// </summary>
            [JsonProperty("customerType")]
            public string CustomerType
            {
                get;
                internal set;
            }
        }
    }
}
