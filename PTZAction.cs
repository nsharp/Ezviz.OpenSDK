﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ezviz.OpenSDK
{
    /// <summary>
    /// 云台操作命令
    ///  一般情况下，鼠标按下代表开始，鼠标松开代表停止
    /// </summary>
    public enum PTZAction
    {
        START,
        STOP
    }
}
