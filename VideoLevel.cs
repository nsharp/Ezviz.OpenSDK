﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ezviz.OpenSDK
{
    /// <summary>
    /// 视频级别
    /// </summary>
    public enum VideoLevel:int
    {
        /// <summary>
        /// 均衡
        /// </summary>
        BALANCED,
        /// <summary>
        /// 流畅
        /// </summary>
        FLUNET,
        /// <summary>
        /// 高清
        /// </summary>
        HD,
        /// <summary>
        /// 超清
        /// </summary>
        SUPERCLEAR,
    }
}
