﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
namespace Ezviz.OpenSDK
{
    /// <summary>
    /// 同步回调参数
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public class NSCBMsg
    {
        public NSCBMsg()
        {
            this.ErrorCode = 0;
            this.ErrorMessage = IntPtr.Zero;
        }

        /// <summary>
        /// 错误代码
        /// </summary>
        public uint ErrorCode;

        /// <summary>
        /// 错误消息
        /// </summary>
        public IntPtr ErrorMessage;
    }
}
