﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
namespace Ezviz.OpenSDK
{
    /// <summary>
    /// 设备列表结果
    /// </summary>
    public class EZDeviceListReponse
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [JsonProperty("resultCode")]
        public int ErrorCode
        {
            get;
            internal set;
        }
        /// <summary>
        /// 设备数量
        /// </summary>
        [JsonProperty("count")]
        public int Count
        {
            get;
            internal set;
        }
        /// <summary>
        /// 摄像头信息
        /// </summary>
        [JsonProperty("cameraList")]
        public Camera[] Cameras
        {
            get;
            internal set;
        }
        /// <summary>
        /// 设备信息
        /// </summary>
        public class Camera
        {
            /// <summary>
            /// 设备在平台的唯一标识
            /// </summary>
            [JsonProperty("deviceId")]
            public string DeviceId
            {
                get;
                internal set;
            }
            /// <summary>
            /// 设备序列号
            /// </summary>
            [JsonProperty("deviceSerial")]
            public string DeviceSerial
            {
                get;
                internal set;
            }
            /// <summary>
            /// 监控点ID
            /// </summary>
            [JsonProperty("cameraId")]
            public string CameraId
            {
                get;
                internal set;
            }
            /// <summary>
            /// 设备通道号
            /// </summary>
            [JsonProperty("cameraNo")]
            public string CameraNo
            {
                get;
                internal set;
            }
            /// <summary>
            ///  监控点名称
            /// </summary>
            [JsonProperty("cameraName")]
            public string CameraName
            {
                get;
                internal set;
            }
            /// <summary>
            /// 设备名称
            /// </summary>
            [JsonProperty("deviceName")]
            public string DeviceName
            {
                get;
                internal set;
            }
            /// <summary>
            /// 
            /// </summary>
            [JsonProperty("display")]
            public string Display
            {
                get;
                internal set;
            }
            /// <summary>
            /// 设备在线状态
            /// </summary>
            [JsonProperty("status")]
            public bool Online
            {
                get;
                internal set;
            }
            /// <summary>
            ///  设备分享状态, 0-未共享;1-共享所有者;2-共享接受者
            /// </summary>
            [JsonProperty("isShared")]
            public int IsShared
            {
                get;
                internal set;
            }
            /// <summary>
            /// 监控点图片url
            /// </summary>
            [JsonProperty("picUrl")]
            public string PicUrl
            {
                get;
                internal set;
            }
            /// <summary>
            /// 加密状态
            /// </summary>
            [JsonProperty("isEncrypt")]
            public bool IsEncrypt
            {
                get;
                internal set;
            }
            /// <summary>
            /// 布撤防状态
            /// </summary>
            [JsonProperty("defence")]
            public bool Defence
            {
                get;
                internal set;
            }
            /// <summary>
            /// 视频清晰度
            /// </summary>
            [JsonProperty("videoLevel")]
            public VideoLevel VideoLevel
            {
                get;
                internal set;
            }
        }
    }
}
